<?php

return[

    'menu_item_all'=>'All',
    'no_team_selected'=>'No team selected',
    'no_teams'=>'No teams',
    'assign.to.team'=>'Assign to another team',
    'assign.to.team.moved'=>'Moved to another team',
    'move.multiple.modal.heading'=>'Move selecteds :label ?'
];