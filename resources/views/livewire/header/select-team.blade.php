<div
    x-data="{
        isDropdownOpen: false,
        direction: @js(__('filament::layout.direction') ?? 'ltr'),
        isRTL() {
            if (this.direction === 'rtl') {
                return true
            }
            return false
        }
    }"
    class="relative"
    :style="(isRTL() && { margin: '0 1rem 0 0' }) || (!isRTL() && { margin: '0 0 0 1rem' })"
>

    <button
        x-on:click="isDropdownOpen = ! isDropdownOpen"
        class="shrink-0 flex items-center justify-center  h-10  mr-2  rounded-lg hover:bg-gray-500/5 lg:mr-2 rtl:lg:mr-0 rtl:lg:ml-2 border border-gray-300"
    >

        <span
            class="flex items-center justify-center h-9 p-2 group-hover:text-white rounded-lg group-focus:text-white bg-white"
        >
            <x-dynamic-component
                component="heroicon-s-users"
                class="w-4 h-4 flex-shrink-0 mr-2 rtl:ml-2 text-primary-500"
            /> {{ $teams[$currentTeam] ??  __('filament-teams::team.no_team_selected')}}
        </span>


    </button>

    <div
        x-show="isDropdownOpen"
        x-on:click.away="isDropdownOpen = false"
        x-transition:enter="transition"
        x-transition:enter-start="-translate-y-1 opacity-0"
        x-transition:enter-end="translate-y-0 opacity-100"
        x-transition:leave="transition"
        x-transition:leave-start="translate-y-0 opacity-100"
        x-transition:leave-end="-translate-y-1 opacity-0"
        class="absolute z-10 right-0 rtl:right-auto rtl:left-0 mt-2 shadow-xl rounded-xl w-52 top-full"
        x-cloak
    >
        <ul class="py-1 space-y-1 overflow-hidden bg-white shadow rounded-xl dark:border-gray-600 dark:bg-gray-700">
            @if ($this->isNoTeamAllowed())
                <li @class([
                    'flex items-center justify-between group px-2',
                    'focus:outline-none hover:text-white focus:text-white hover:bg-gray-500 focus:bg-primary-700 cursor-pointer' => $currentTeam != null,
                ])>
                    <a
                        wire:click="changeTeam(null)"
                        class="flex items-center w-full h-8 px-2 text-sm font-medium whitespace-nowrap filament-dropdown-item"
                    >
                        <span class="truncate">
                            {{__('filament-teams::team.menu_item_all')}}
                        </span>
                    </a>
                    @if ($currentTeam == null)
                        <x-dynamic-component
                            component="heroicon-s-check"
                            class="w-4 h-4 flex-shrink-0 mr-2 rtl:ml-2 text-primary-500"
                        />
                    @endif
                </li>
            @endif
            @forelse ($teams as $key => $team)
                <li @class([
                    'flex items-center justify-between group px-2',
                    'focus:outline-none hover:text-white focus:text-white hover:bg-gray-500 focus:bg-primary-700 cursor-pointer' =>
                        $currentTeam != $key,
                ])>
                    <a
                        wire:click="changeTeam('{{ $key }}')"
                        class="flex items-center w-full h-8 px-2 text-sm font-medium whitespace-nowrap filament-dropdown-item"
                    >
                        <span class="truncate">
                            {{ $team }}
                        </span>
                    </a>
                    @if ($currentTeam == $key)
                        <x-dynamic-component
                            component="heroicon-s-check"
                            class="w-4 h-4 flex-shrink-0 mr-2 rtl:ml-2 text-primary-500"
                        />
                    @endif
                </li>
            @empty
            
            {{__('filament-teams::team.no_teams')}}
            @endforelse

        </ul>
    </div>
</div>
