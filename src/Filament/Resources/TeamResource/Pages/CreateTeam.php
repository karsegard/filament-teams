<?php

namespace KDA\Filament\Teams\Filament\Resources\TeamResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use KDA\Filament\Teams\Filament\Resources\TeamResource;

class CreateTeam extends CreateRecord
{
    protected static string $resource = TeamResource::class;
}
