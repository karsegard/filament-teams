<?php

namespace KDA\Filament\Teams\Filament\Resources\TeamResource\Pages;

use Filament\Resources\Pages\EditRecord;
use KDA\Filament\Teams\Filament\Resources\TeamResource;

class EditTeam extends EditRecord
{
    protected static string $resource = TeamResource::class;
}
