<?php

namespace KDA\Filament\Teams\Filament\Forms\Actions;

use Filament\Forms\Components\Select;
use Filament\Tables\Actions\BulkAction;
use KDA\Laravel\Teams\Facades\Team;
use KDA\Laravel\Teams\Models\Team as ModelsTeam;

class AssignToTeamBulkAction extends BulkAction{
    public static function getDefaultName(): ?string
    {
        return 'assign_to_team';
    }
    protected function setUp(): void
    {
        parent::setUp();

        $this->label(__('filament-teams::team.assign.to.team'));

        $this->modalHeading(fn (): string => __('filament-teams::team.move.multiple.modal.heading', ['label' => $this->getPluralModelLabel()]));

        $this->modalButton(__('filament-support::actions/delete.multiple.modal.actions.delete.label'));

        $this->successNotificationTitle(__('filament-teams::team.assign.to.team.moved'));

        $this->color('warning');

        $this->icon('heroicon-o-arrow-circle-right');

        $this->requiresConfirmation();
        $this->form(function(){
            return [
                Select::make('team_id')->options(Team::getTeams()->pluck('name','id'))->required()
            ];
        });

        $this->action(function ($data,$records): void {
            $team_id = $data['team_id'];
            $team = ModelsTeam::find($team_id);
            $records->each(function($record) use($team){
                Team::moveOwnership($record,$team);
            });
           // $this->process(static fn (Collection $records) => $records->each(fn (Model $record) => Team::registerOwnership($record,)));

            $this->success();
        });

        $this->deselectRecordsAfterCompletion();

       
    }
}
