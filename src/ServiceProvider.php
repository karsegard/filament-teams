<?php

namespace KDA\Filament\Teams;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasProviders;
use KDA\Laravel\Traits\HasTranslations;
use KDA\Laravel\Traits\HasViews;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasProviders;
    use HasTranslations;
    use HasViews;

    protected $packageName = 'filament-teams';

    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

        //register filament provider
    protected $additionnalProviders = [
        \KDA\Filament\Teams\FilamentServiceProvider::class,
    ];

    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
