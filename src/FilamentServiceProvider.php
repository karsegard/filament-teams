<?php

namespace KDA\Filament\Teams;

use Filament\Facades\Filament;
use Filament\PluginServiceProvider;
use Illuminate\Support\Facades\Blade;
use KDA\Filament\Teams\Filament\Resources\TeamResource;
use KDA\Filament\Teams\Filament\Resources\TeamResource\RelationManagers\MembersRelationManager;
use KDA\Filament\Teams\Livewire\Header\SelectTeam;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
        //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
        //    CustomWidget::class,
    ];

    protected array $pages = [
        //    CustomPage::class,
    ];

    protected array $resources = [
        TeamResource::class,
        //     CustomResource::class,
    ];

    protected array $relationManagers = [
        MembersRelationManager::class
    ];
    public function configurePackage(Package $package): void
    {
        $package->name('filament-teams');
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
        Livewire::component('switch-filament-teams', SelectTeam::class);

        Filament::registerRenderHook(
            'global-search.start',
            fn (): string => Blade::render("@livewire('switch-filament-teams')")
        );
        
    }
}
