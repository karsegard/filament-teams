<?php

namespace KDA\Filament\Teams\Livewire\Header;

use KDA\Laravel\Teams\Facades\Team;
use Livewire\Component;

class SelectTeam extends Component
{
    public $teams = [

    ];

    public $currentTeam;

    public function mount()
    {
        $this->teams = Team::getTeams()?->pluck('name','id') ?? [];
        $this->currentTeam = Team::getCurrentTeam()?->id;
    }

    public function render()
    {
        return view('filament-teams::livewire.header.select-team');
    }

    public function changeTeam($team)
    {
        //session()->put('team', $team);
        Team::setCurrentTeam($team);

        $this->redirect(request()->header('Referer'));
    }

    public function clearCurrentTeam($team)
    {
        //session()->put('team', $team);
        Team::clearCurrentTeam($team);
        
        $this->redirect(request()->header('Referer'));
    }
    public function isNoTeamAllowed(){
        return Team::isNoTeamAllowed();
    }
}
